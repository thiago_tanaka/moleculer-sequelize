"use strict";

const Sequelize = require("sequelize");
const SequelizeMixin = require("../mixins/db.mixin");

module.exports = {
	name: "items",
	mixins: [SequelizeMixin],
	settings: {
		include: "user"
	},
	model: {
		name: "items",
		define: {
			id: {
				primaryKey: true,
				autoIncrement: true,
				type: Sequelize.INTEGER
			},
			name: Sequelize.STRING,
			userTable: Sequelize.INTEGER,
		},
		association: [{
			model: "users",
			relationship: "belongsTo",
			foreignKey: 'userTable',
		}],
		options: {
			paranoid: true,
		}
	},

	actions: {
		list: {},
	},
};
